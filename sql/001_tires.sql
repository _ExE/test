CREATE TABLE IF NOT EXISTS `tires` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL COMMENT 'названия шин',
  `is_problem` tinyint(1) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `is_problem` (`is_problem`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COMMENT='исходная таблица шин';


INSERT INTO `tires` (`id`, `name`, `is_problem`) VALUES
(1, 'Toyo H08 195/75R16C 107/105S TL Летние', NULL),
(2, 'Pirelli Winter SnowControl serie 3 175/70R14 84T TL Зимние (нешипованные)', NULL),
(3, 'BFGoodrich Mud-Terrain T/A KM2 235/85R16 120/116Q TL Внедорожные', NULL),
(4, 'Pirelli Scorpion Ice & Snow 265/45R21 104H TL Зимние (нешипованные)', NULL),
(5, 'Pirelli Winter SottoZero Serie II 245/45R19 102V XL Run Flat * TL Зимние (нешипованные)', NULL),
(6, 'Nokian Hakkapeliitta R2 SUV/Е 245/70R16 111R XL TL Зимние (нешипованные)', NULL),
(7, 'Pirelli Winter Carving Edge 225/50R17 98T XL TL Зимние (шипованные)', NULL),
(8, 'Continental ContiCrossContact LX Sport 255/55R18 105H FR MO TL Всесезонные', NULL),
(9, 'BFGoodrich g-Force Stud 205/60R16 96Q XL TL Зимние (шипованные)', NULL),
(10, 'BFGoodrich Winter Slalom KSI 225/60R17 99S TL Зимние (нешипованные)', NULL),
(11, 'Continental ContiSportContact 5 245/45R18 96W SSR FR TL Летние', NULL),
(12, 'Continental ContiWinterContact TS 830 P 205/60R16 92H SSR * TL Зимние (нешипованные)', NULL),
(13, 'Continental ContiWinterContact TS 830 P 225/45R18 95V XL SSR FR * TL Зимние (нешипованные)', NULL),
(14, 'Hankook Winter I*Cept Evo2 W320 255/35R19 96V XL TL/TT Зимние (нешипованные)', NULL),
(15, 'Mitas Sport Force+ 120/65R17 56W TL Летние', NULL);


CREATE TABLE IF NOT EXISTS `characteristics` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `tire_id` int(10) UNSIGNED NOT NULL COMMENT 'id таблицы названий',
  `characteristic_group` set('бренд','модель','ширина','высота','конструкция','диаметр','индекс нагрузки','индекс скорости','характеризующие аббревиатуры','ранфлэт','камерность','сезон') NOT NULL COMMENT 'тип характеристики',
  `val` varchar(255) NOT NULL COMMENT 'значение характеристики',
  PRIMARY KEY (`id`),
  UNIQUE KEY `tire_id_2` (`tire_id`,`characteristic_group`),
  KEY `tire_id` (`tire_id`),
  KEY `characteristic_group` (`characteristic_group`),
  KEY `val` (`val`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='харакетеристики';
