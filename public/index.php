<?php

session_start();
/*** define the site path ***/
define('__SITE_PATH', realpath(dirname(__DIR__)));

/*** include the init.php file ***/
include_once __SITE_PATH . '/inc/init.php';


function exeClasslLoader($class_name)
{
    $file = __CLASS_PATH . strtolower($class_name) . '.class.php';

    if (file_exists($file) == true) {
        include_once $file;
        return true;
    } else {
        $dirlist = glob(__CLASS_PATH . "*", GLOB_ONLYDIR);
        foreach ($dirlist as $v) {

            $file = $v . '/' . strtolower($class_name) . '.class.php';

            if (file_exists($file) == true) {
                include_once $file;
                return true;
            }
        }
    }
    return false;
}

spl_autoload_register('exeClasslLoader');

//PSR-0
//https://jeremycurny.com/2016/06/30/php-psr-4-autoloader/
spl_autoload_register(function ($class) {
    $file = __SITE_PATH . str_replace('\\', '/', $class) . '.php';
    if (file_exists($file)) {
        require $file;
    }
});

router::do_rout(__CONTROLLER_PATH);

?>