<?php if (!defined('__SITE_PATH')) exit('No direct script access allowed');

class db
{
    /*** Declare instance ***/
    private static $instance = NULL;
    /**
     *
     * the constructor is set to private so
     * so nobody can create a new instance using new
     *
     */
    private function __construct()
    {
        /*** maybe set the db name here later ***/
    }

    /**
     *
     * Return DB instance or create intitial connection
     * @return object (PDO)
     * @access public
     */
    public static function getInstance()
    {
        //http://phpfaq.ru/pdo
        if (!self::$instance) {
            $dsn = "mysql:host=" . __DB_HOST . ";dbname=" . __DB_NAME . ";charset=" . __DB_CHARSET;
            $opt = array(
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
            );
            try {
                self::$instance = new PDO($dsn, __DB_USER, __DB_PASSWORD, $opt);
            } catch (PDOException $e) {
                echo "Хьюстон, у нас проблемы на первой базе.";
                file_put_contents(__ERROR_LOG, $e->getMessage(), FILE_APPEND);
            }
        }
        return self::$instance;
    }

    /**
     *
     * Like the constructor, we make __clone private
     * so nobody can clone the instance
     *
     */
    private function __clone()
    {
    }

}

/*** end of class ***/

?>