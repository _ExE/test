<?php if (!defined('__SITE_PATH')) exit('No direct script access allowed');

class router
{
    /*
    * @the controller path
    */
    static private $path;
    static public $file;
    static public $controller;
    static public $action;
    static public $param;

    static public function do_rout($path)
    {
        self::setPath($path);
        self::getController();
        self::run_action();
    }


    static private function setPath($path)
    {
        if (is_dir($path) == false) {
            throw new Exception ("Invalid controller path: $path");
        }
        self::$path = $path;
    }

    static private function getControllerCron()
    {

        if (isset($_SERVER["argv"][1])) {
            self::$controller = $_SERVER["argv"][1];
        } else {
            self::$controller = 'index';
        }

        if (isset($_SERVER["argv"][2])) {
            self::$action = $_SERVER["argv"][2];
        } else {
            self::$action = 'index';
        }

        if (isset($_SERVER["argv"][3])) {
            self::$param[] = $_SERVER["argv"][3];
        } else {
            self::$param[] = '';
        }

        if (isset($_SERVER["argv"][4])) {
            self::$param[] = $_SERVER["argv"][4];
        }

        self::$file = self::$path . self::$controller . '.c.php';

    }

    static public function control_sum($param)
    {

        $control_sum = '';
        foreach ($param as $k => $v) {
            if ($k === 'cc') continue;
            $control_sum .= md5($v);
        }
        return md5($control_sum . __SALT);
    }


    static private function getController()
    {

        $flag_index = false;
        $flag_error = false;


//echo("<pre>");
//print_r($_GET);
//echo("</pre>");

//echo("<pre>");
//print_r($_POST);
//echo("</pre>");
//exit;


        if (!empty($_GET['rt'])) {
            if (preg_match("/^[_0-9a-zA-Z.\/\-]+$/i", $_GET['rt'])) {
                $parts = explode('/', $_GET['rt']);
            } else {
                $flag_error = true;
            }
        } else {
            $flag_index = true;
        }

        if (!empty($_POST['cc'])) {
            if ($_POST['cc'] != self::control_sum($_POST)) {
                $flag_error = true;
            }
        }


        if ((!$flag_index) && (!$flag_error)) {
            switch (count($parts)) {
                case 0:
                    //эта ветка не должна выполняться
                    $flag_index = true;
                    break;
                case 1:
                    self::$controller = $parts[0];
                    self::$action = 'index';
                    self::$param[] = '';
                    break;
                case 2:
                    self::$controller = $parts[0];
                    self::$action = $parts[1];
                    self::$param[] = '';
                    break;
                case 3:
                    self::$controller = $parts[0];
                    self::$action = $parts[1];
                    self::$param[] = $parts[2];
                    break;
                case 4:
                    self::$controller = $parts[0];
                    self::$action = $parts[1];
                    self::$param[] = $parts[2];
                    self::$param[] = $parts[3];
                    break;
                case 5:
                    self::$controller = $parts[0];
                    self::$action = $parts[1];
                    self::$param[] = $parts[2];
                    self::$param[] = $parts[3];
                    self::$param[] = $parts[4];
                    break;
                case 6:
                    self::$controller = $parts[0];
                    self::$action = $parts[1];
                    self::$param[] = $parts[2];
                    self::$param[] = $parts[3];
                    self::$param[] = $parts[4];
                    self::$param[] = $parts[5];
                    break;
                case 7:
                    self::$controller = $parts[0];
                    self::$action = $parts[1];
                    self::$param[] = $parts[2];
                    self::$param[] = $parts[3];
                    self::$param[] = $parts[4];
                    self::$param[] = $parts[5];
                    self::$param[] = $parts[6];
                    break;
                case 8:
                    self::$controller = $parts[0];
                    self::$action = $parts[1];
                    self::$param[] = $parts[2];
                    self::$param[] = $parts[3];
                    self::$param[] = $parts[4];
                    self::$param[] = $parts[5];
                    self::$param[] = $parts[6];
                    self::$param[] = $parts[7];
                    break;
            }
        }
        self::$file = self::$path . self::$controller . '.c.php';

        if (is_readable(self::$file) === false) {
            $flag_error = true;
        }

        if ($flag_error) {
            //или вариант со страницей 404
            self::$file = self::$path . 'error404.c.php';
            self::$controller = 'error404';
        }

        if ($flag_index) {
            //откроем index
            self::$controller = 'index';
            self::$file = self::$path . 'index.c.php';
        }


//echo("<pre>");
//print_r("**4**");
//echo("</pre>");

//echo("<pre>");
//print_r(self::$file);
//echo("</pre>");

//echo("<pre>");
//print_r(self::$controller);
//echo("</pre>");

//echo("<pre>");
//print_r(self::$action);
//echo("</pre>");

//echo("<pre>");
//print_r(self::$param);
//echo("</pre>");


    }

    static private function run_action()
    {

        /*** include the controller ***/
        include self::$file;

        /*** a new controller class instance ***/
        $class = self::$controller . '_controller';
        $controller = new $class();

        /*** check if the action is callable ***/
        if (is_callable(array($controller, self::$action)) == false) {
            $action = 'index';
        } else {
            $action = self::$action;
        }
        /*** run the action ***/
        $controller->$action(self::$param);
    }
}

?>