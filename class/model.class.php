<?php  if (!defined('__SITE_PATH')) exit('No direct script access allowed');

Class model {

	/**
	 *
	 * @constructor
	 *
	 * @access public
	 *
	 * @return void
	 *
	 */
	function __construct() {

	}

	//фабричный метод создания класса модели
	function create($name) {

		$file = __MODEL_PATH . $name . '.m.php';
		if (!file_exists($file)) return false;
		include_once($file);
		
		return new $name();
	}
}
?>