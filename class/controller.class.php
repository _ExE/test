<?php  if (!defined('__SITE_PATH')) exit('No direct script access allowed');

Abstract Class controller {

    protected $template;
	/** @var array $errors накапливат все ошибки по ходу выполнения  */
    protected $errors;
    /** @var array $msg накапливат все сообщения (не ошибки) */
    protected $msg;


	public function __construct($withoutAuthorization = false) {
		$this->template = new template();
		$this->model = new model();
	}

	/**
	 * @all controllers must contain an index method
	 */
	abstract function index($param);

	function __destruct() {
	}

	protected function hdr($kod,$ar){
		switch ($kod) {
		    case '400':
				header('Status: 400 Bad Request', false, 400);
				header($_SERVER['SERVER_PROTOCOL'].' 400 Bad Request', false, 400);		
				echo json_encode(['error'=>true,'msg'=>$ar]);
		        break;
		    case '200':
				header('Status: 200 OK', false, 200);
				header($_SERVER['SERVER_PROTOCOL'].' 200 OK', false, 200);			
				echo json_encode(['ok'=>true,'msg'=>$ar]);
		        break;
		}
		exit;
	}	

}
?>