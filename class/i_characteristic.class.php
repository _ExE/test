<?php

// Объявим интерфейс 'i_characteristic'
interface i_characteristic
{
    public function checkVal($val);
    public function isRequired();
    public function getName();
}