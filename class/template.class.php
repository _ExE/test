<?php  if (!defined('__SITE_PATH')) exit('No direct script access allowed');

Class template {

	/*
	 * @Variables array
	 * @access private
	 */
	private $vars = array();

	/**
	 *
	 * @constructor
	 *
	 * @access public
	 *
	 * @return void
	 *
	 */
	function __construct() {

	}


	 /**
	 *
	 * @set undefined vars
	 *
	 * @param string $index
	 *
	 * @param mixed $value
	 *
	 * @return void
	 *
	 */
	 public function __set($index, $value){

			$this->vars[$index] = $value;
	 }

	
	public function __get($name){
		if (array_key_exists($name, $this->vars)) {
			return $this->vars[$name];
		}
		$trace = debug_backtrace();
		trigger_error(
			'Неопределенное свойство в __get(): ' . $name .
			' в файле ' . $trace[0]['file'] .
			' на строке ' . $trace[0]['line'],
			E_USER_NOTICE);
		return null;
	}

	public function __isset($name)	{
		return isset($this->vars[$name]);
	}

	public function __unset($name){
		unset($this->vars[$name]);
	}


	//выполняет скрипт - вывод на экран
	public function show($name) {

		$path = __VIEWS_PATH . $name . '.v.php';

		if (file_exists($path) == false)
		{
			throw new Exception("Template not found in $path");
			return false;
		}

		// Load variables
		foreach ($this->vars as $key => $value)
		{
			$$key = $value;
		}

		include ($path);
	}


	//выполняет скрипт - вывод в return
	public function get_content($name) {

		$path = __VIEWS_PATH . $name . '.v.php';

		if (file_exists($path) == false)
		{
			throw new Exception("Template not found in $path");
			return false;
		}

		// Load variables
		foreach ($this->vars as $key => $value)
		{
			$$key = $value;
		}

		ob_start();
		include ($path);  
		$res = ob_get_contents();
		ob_end_clean();

		return $res;               
	}

	//выполняет скрипт который должен содержать return
	public function get_var($name) {

		$path = __VIEWS_PATH . $name . '.v.php';

		if (file_exists($path) == false)
		{
			throw new Exception("Template not found in $path");
			return false;
		}

		// Load variables
		foreach ($this->vars as $key => $value)
		{
			$$key = $value;
		}

		return include ($path);  
	}



	public function mount_vars($content,$ar){
		foreach ($ar as $k=>$v){ 
			$content = str_replace('{'.$k.'}',$v,$content);
		}
	}
}

?>