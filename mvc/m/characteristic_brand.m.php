<?php if (!defined('__SITE_PATH')) exit('No direct script access allowed');

class characteristic_brand implements i_characteristic
{
    private $arRequiredVal = [
        'Nokian',
        'BFGoodrich',
        'Pirelli',
        'Toyo',
        'Continental',
        'Hankook',
        'Mitas',
    ];

    private $characteristic_name = 'бренд';

    /**
     * проверяет допустимость значения
     * @param $val
     * @return bool
     */
    public function checkVal($val){
        if(in_array($val,$this->arRequiredVal))
            return true;
        return false;
    }

    /**
     * обязательна или нет эта характеристика
     * @return bool
     */
    public function isRequired(){
        return true;
    }

    public function getName(){
        return $this->characteristic_name;
    }

}
