<?php if (!defined('__SITE_PATH')) exit('No direct script access allowed');

class characteristic_model implements i_characteristic
{

    private $characteristic_name = 'модель';

    /**
     * проверяет допустимость значения
     * @param $val
     * @return bool
     */
    public function checkVal($val)
    {
        return true;
    }

    /**
     * обязательна или нет эта характеристика
     * @return bool
     */
    public function isRequired()
    {
        return true;
    }

    public function getName()
    {
        return $this->characteristic_name;
    }

}
