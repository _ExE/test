<?php if (!defined('__SITE_PATH')) exit('No direct script access allowed');

class characteristics extends db_main
{

    private $table; //имя таблицы

    public function __construct()
    {
        $this->db = db::getInstance(); // connect to database

        $this->table = 'characteristics';
    }


    /**
     * добавляет записи характеристик в таблицу
     * @param $tire_id
     * @param $ar
     * @return bool
     */
    public function add($tire_id, $ar)
    {

        $sql = "
		INSERT INTO
			{$this->table} 
		SET
			tire_id = :tire_id,
			characteristic_group = :characteristic_group,
			val = :val
		";
        $st = $this->db->prepare($sql);

        foreach($ar as $k=>$v){
            try {
                $st->execute([
                    'tire_id'=>$tire_id,
                    'characteristic_group'=>$k,
                    'val'=>$v,
                    ]);
            } catch (Exception $e) {
                $this->errors[] = $e->getMessage();
                error_log($e->getMessage(), 0);
                return false;
            }
        }
        return true;
    }



}

?>