<?php if (!defined('__SITE_PATH')) exit('No direct script access allowed');

class characteristic_season implements i_characteristic
{
    private $arRequiredVal = [
        'Зимние (шипованные)',
        'Внедорожные',
        'Летние',
        'Зимние (нешипованные)',
        'Всесезонные',
    ];

    private $characteristic_name = 'сезон';

    /**
     * проверяет допустимость значения
     * @param $val
     * @return bool
     */
    public function checkVal($val){
        if(in_array($val,$this->arRequiredVal))
            return true;
        return false;
    }

    /**
     * обязательна или нет эта характеристика
     * @return bool
     */
    public function isRequired(){
        return true;
    }

    public function getName(){
        return $this->characteristic_name;
    }

}
