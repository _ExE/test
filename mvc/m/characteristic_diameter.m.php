<?php if (!defined('__SITE_PATH')) exit('No direct script access allowed');

class characteristic_diameter implements i_characteristic
{

    private $characteristic_name = 'диаметр';

    /**
     * проверяет допустимость значения
     * @param $val
     * @return bool
     */
    public function checkVal($val)
    {
        if (is_numeric($val))
            return true;
        return false;
    }

    /**
     * обязательна или нет эта характеристика
     * @return bool
     */
    public function isRequired()
    {
        return true;
    }

    public function getName()
    {
        return $this->characteristic_name;
    }

}
