<?php if (!defined('__SITE_PATH')) exit('No direct script access allowed');

class characteristic_camera implements i_characteristic
{
    private $arRequiredVal = [
        'ТТ',
        'TL',
        'TL/TT',
    ];

    private $characteristic_name = 'камерность';

    /**
     * проверяет допустимость значения
     * @param $val
     * @return bool
     */
    public function checkVal($val){
        if(in_array($val,$this->arRequiredVal))
            return true;
        return false;
    }

    /**
     * обязательна или нет эта характеристика
     * @return bool
     */
    public function isRequired(){
        return false;
    }

    public function getName(){
        return $this->characteristic_name;
    }

}
