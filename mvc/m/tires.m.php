<?php if (!defined('__SITE_PATH')) exit('No direct script access allowed');

class tires extends db_main
{

    private $table; //имя таблицы

    public function __construct()
    {
        $this->db = db::getInstance(); // connect to database

        $this->table = 'tires';
    }

    /**
     * получат все новые (не обработанные записи)
     * @return mix
     */
    public function allNew()
    {

        $sql = "
		SELECT 
			*
		FROM 
			{$this->table} 
		WHERE
			is_problem IS NULL 
        ";
        try {
            $st = $this->db->prepare($sql);
            $st->execute();
            return $st->fetchAll(PDO::FETCH_ASSOC);
        } catch (Exception $e) {
            $this->errors[] = $e->getMessage();
            error_log($e->getMessage(), 0);
            return false;
        }
    }


    /**
     * устанавливает флаг реузультата парсинга наименования
     * @param $id id записи в которой нужно установить флаг
     * @param $flag значение флага
     * @return bool
     */
    public function setIsProblem($id, $flag)
    {
        $sql = "
		UPDATE
			{$this->table} 
		SET
		  is_problem = :flag
		WHERE 
		  id = :id
		LIMIT 1;
		";

        try {
            $st = $this->db->prepare($sql);
            $st->execute(['flag' => $flag, 'id' => $id]);
            return true;
        } catch (Exception $e) {
            $this->errors[] = $e->getMessage();
            error_log($e->getMessage(), 0);
            return false;
        }
    }


}

?>