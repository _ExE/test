<?php if (!defined('__SITE_PATH')) exit('No direct script access allowed');

Class index_controller Extends controller
{
    private $modelTires;
    private $modelCharacteristics;

    //характеристики
    private $cBrand;
    private $cModel;
    private $cWidth;
    private $cHeight;
    private $cDiameter;
    private $cLoad;
    private $cSpeed;
    private $cSeason;
    private $cRunFlat;
    private $cCamera;
    private $cAbbreviation;

    function __construct()
    {
        parent::__construct();

        $this->modelTires = $this->model->create('tires');
        $this->modelCharacteristics = $this->model->create('characteristics');

        $this->cBrand = $this->model->create('characteristic_brand');
        $this->cModel = $this->model->create('characteristic_model');
        $this->cWidth = $this->model->create('characteristic_width');
        $this->cHeight = $this->model->create('characteristic_height');
        $this->cDiameter = $this->model->create('characteristic_diameter');
        $this->cLoad = $this->model->create('characteristic_load_index');
        $this->cSpeed = $this->model->create('characteristic_speed_index');
        $this->cSeason = $this->model->create('characteristic_season');
        $this->cRunFlat = $this->model->create('characteristic_run_flat');
        $this->cCamera = $this->model->create('characteristic_camera');
        $this->cAbbreviation = $this->model->create('characteristic_abbreviation');
    }

    public function index($param)
    {

        /** @var $newlTires - массив новых (еще не обработанных записей) из таблицы наименований шин */
        $newlTires = $this->modelTires->allNew();

        foreach ($newlTires as $v) {
            if ($this->doParse($v['id'],$v['name']))
                $this->modelTires->setIsProblem($v['id'], 0);
            else
                $this->modelTires->setIsProblem($v['id'], 1);

        }

        $this->template->newlTires = $newlTires;

        $this->template->show('index');

    }

    /**
     * парсинг названия
     * @param $id
     * @param $name
     * @return bool
     */
    private function doParse($id, $name)
    {

        $name = trim($name);
        $name = str_replace('  ', ' ', $name);
        $ar = explode(' ', $name);
        $arRes = [];

//        echo('<pre>');
//        print_r($ar);

        //бренд
        $cName = $this->cBrand->getName();
        $arRes[$cName] = array_shift($ar);
        if (!$this->cBrand->checkVal($arRes[$cName])){
//            echo($arRes[$cName]);
            return false;
        }

        //модель
        $cName = $this->cModel->getName();
        $ar_model = [];
        $nextTxt = '';
        foreach ($ar as $v) {
            if (strstr($v, '/'))
                if ((strstr($v, 'R1'))||(strstr($v, 'R2'))) {
                    $nextTxt = array_shift($ar);
                    break;
                }
            $ar_model[] = array_shift($ar);
        }
        if ($this->cModel->isRequired())
            if (count($ar_model) == 0){
//                echo(1);
                return false;
            }
        if ($nextTxt == ''){
//            echo(2);
            return false;
        }
        $arRes[$cName] = implode(' ', $ar_model);

        //группа характеристик без пробелов
        $ar2 = explode('/', $nextTxt);
        if (count($ar2) < 2){
//            echo(3);
            return false;
        }

        //ширина
        $cName = $this->cWidth->getName();
        $arRes[$cName] = array_shift($ar2);
        if (!$this->cWidth->checkVal($arRes[$cName])){
//            echo($arRes[$cName]);
            return false;
        }

        //до и после R
        //в описании и в примерах нет других вариантов кроме R, судя по всему это радиальный крорд и возможны иные варианты (теоретически)
        //но какие не понятно, пока висключчил ее списка характеристик, так как нет сымысла ставит всем  R
        $ar2 = explode('R', array_shift($ar2));
        if (count($ar2) < 2){
//            echo(4);
            return false;
        }

        //высота
        $cName = $this->cHeight->getName();
        $arRes[$cName] = array_shift($ar2);
        if (!$this->cHeight->checkVal($arRes[$cName])){
//            echo($arRes[$cName]);
            return false;
        }
        //диаметр
        $cName = $this->cDiameter->getName();
        $arRes[$cName] = array_shift($ar2);
        if (!$this->cDiameter->checkVal($arRes[$cName])){
//            echo($arRes[$cName]);
            return false;
        }

        $nextTxt = array_shift($ar);

        //индекс нагрузки
        $cName = $this->cLoad->getName();
        $arRes[$cName] = preg_split("/[a-zA-Z]+/", $nextTxt)[0];
        if (!$this->cLoad->checkVal($arRes[$cName])){
//            echo($arRes[$cName]);
            return false;
        }
        $cLoad = $arRes[$cName];


        //индекс скорости
        $cName = $this->cSpeed->getName();
        $arRes[$cName] = explode($cLoad, $nextTxt)[1];
        if (!$this->cSpeed->checkVal($arRes[$cName])){
//            echo($arRes[$cName]);
            return false;
        }


        //сезон
        $cName = $this->cSeason->getName();
        $arRes[$cName] = array_pop($ar);
        if (($arRes[$cName] == '(шипованные)') || ($arRes[$cName] == '(нешипованные)'))
            $arRes[$cName] = array_pop($ar) . ' ' . $arRes[$cName];

        if (!$this->cSeason->checkVal($arRes[$cName])){
//            echo($arRes[$cName]);
            return false;
        }


        //необязательные параметры
        $nextTxt = array_pop($ar);
        //камерность
        if ($this->cCamera->checkVal($nextTxt)) {
            $arRes[$this->cCamera->getName()] = $nextTxt;
            if (count($ar) > 0)
                $nextTxt = array_pop($ar);
        }
        //ранфлэт
        if($nextTxt=='Flat')
            $arRes[$cName] = array_pop($ar) . ' ' . $arRes[$cName];

        if ($this->cRunFlat->checkVal($nextTxt)) {
            $arRes[$this->cRunFlat->getName()] = $nextTxt;
            if (count($ar) > 0)
                $nextTxt = array_pop($ar);
        }

        if(count($ar)>0){
            $nextTxt = array_shift($ar);
            $arRes[$this->cAbbreviation->getName()] = $nextTxt;
        }

        $this->modelCharacteristics->add($id,$arRes);

//        echo('<pre>');
//        print_r($arRes);
//        echo('</pre>');

        return true;
    }

}

?>